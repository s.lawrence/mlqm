#!/usr/bin/env python

"""
TODO

* Replace adam's learning rate (for energy minimization) with a schedule
* Symmetric wavefunction
* A model with indistinguishable bosons (repulsive, in a box?)
* Symmetric normalizing flow
* Use unbiased estimate of energy at the end.


"""

from functools import partial
import sys
import time
from typing import Callable

import equinox as eqx
import equinox.nn as nn
import jax
import jax.numpy as jnp
import optax
jax.config.update('jax_platform_name', 'cpu')

from flow import *
from util import *

S = 1000
N = 4
D = 1
m2 = 1.
lamda = 2.

class DistinguishableWavefunction(eqx.Module):
    N: int
    D: int
    sigma: jnp.array
    mlp: nn.MLP

    def __init__(self, key, N, D, depth):
        self.mlp = nn.MLP(in_size=N*D, out_size=2, width_size=2*N*D, depth=depth, key=key)
        self.sigma = jnp.array(1.)
        self.N = N
        self.D = D

    def __call__(self, x):
        assert x.shape == (N,D)
        x = x.reshape(N*D)
        wf_ri = self.mlp(x)
        window = jnp.exp(-jnp.sum(x**2)/(2. * self.sigma**2))
        return (wf_ri[0] + 1j*wf_ri[1]) * window

class BosonicWavefunction:
    N: int
    D: int
    C: int
    sigma: jnp.array
    coord: list
    mlp: nn.MLP

    def __init__(self, key, N, D, depth):
        self.sigma = jnp.array(1.)
        ckey, mkey = jax.random.split(key)

        # Coordinates
        def _make_coordinate(k):
            return nn.MLP(
                    in_size = D,
                    out_size = 1,
                    width_size = 2*D,
                    depth = 2,
                    key = k
            )
        C = 2*N*D
        self.C = C
        self.N = N
        self.D = D
        self.coord = [_make_coordinate(k) for k in jax.random.split(ckey, C)]

        # The wavefunction itself
        self.mlp = nn.MLP(
                in_size = C,
                out_size = 2,
                width_size = C,
                depth = depth,
                key = mkey
        )

    def __call__(self, x):
        N,D = self.N,self.D

        # First compute the C coordinates
        def symmetric_coordinate(i):
            return jnp.mean(jax.vmap(self.coord[i])(x))
        y = jnp.zeros(self.C)
        for i in range(self.C):
            y.at[i].set(symmetric_coordinate(i))

        wf_ri = self.mlp(y)
        window = jnp.exp(-jnp.sum(x**2)/(2. * self.sigma**2))
        return (wf_ri[0] + 1j*wf_ri[1]) * window

class FermionicWavefunction:
    pass

class Wavefunction:
    # TODO support an arbitrary mix of statistics
    pass


def sample(xk):
    return jax.random.normal(xk, (S,N))

# Flow loss function
@eqx.filter_jit
@partial(eqx.filter_value_and_grad, has_aux=True)
def flow_loss(flow, wf, zk):
    z = sample(zk)
    x, lpsamp = jax.vmap(flow)(z)
    x = x.reshape((S,N,D))
    psamp = jnp.exp(lpsamp)
    psi = jax.vmap(wf)(x)
    prob = (psi.conj()*psi).real
    rew = prob/psamp
    diff = jnp.log(prob) - jnp.log(psamp)
    kl = jnp.mean(rew * diff) / jnp.mean(rew)
    merit = jnp.mean(rew*diff*diff) / jnp.mean(rew) - kl**2
    return kl, merit

def potential_ho(x):
    return jnp.sum(x**2)/2.

def potential_aho(x, m2, lamda):
    assert x.shape[1] == 1
    # TODO quadratic all-to-all
    return m2 * jnp.sum(x**2)/2. + lamda*jnp.sum(x**4)

def potential_ON(x, m2, lamda):
    assert x.shape[1] == 1
    N = x.shape[0]
    return m2 * jnp.sum(x**2)/2. + lamda / N * jnp.sum(x**2)**2

# Hamiltonian loss (i.e. energy expectation value)
def energy_function(potential, *args, **kwargs):
    """ Return a grad'd and jit'd energy function for a given potential. """
    def energy(wf, flow, zk):
        z = sample(zk)
        x, lpsamp = jax.vmap(flow)(z)
        x = x.reshape((S,N,D))
        psamp = jnp.exp(lpsamp)

        def dwf(x):
            wf_r = lambda y: wf(y).real
            wf_i = lambda y: wf(y).imag
            return jax.grad(wf_r)(x) + 1j * jax.grad(wf_i)(x)

        psi = jax.vmap(wf)(x)
        dpsidx = jax.vmap(dwf)(x)
        kin = jnp.sum((dpsidx.conj() * dpsidx)/2., axis=(1,2))
        pot = psi.conj() * jax.vmap(lambda y: potential(y, *args, **kwargs))(x) * psi
        ham = kin + pot
        prob = psi.conj() * psi
        return jnp.mean((ham/psamp).real) / jnp.mean((prob/psamp).real)

    return eqx.filter_jit(eqx.filter_value_and_grad(energy))

if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(
            description="Train NFQS",
            formatter_class=argparse.ArgumentDefaultsHelpFormatter,
            fromfile_prefix_chars='@'
            )
    parser.add_argument('--debug-nan', action='store_true',
            help='nan debugging mode')
    parser.add_argument('--debug-inf', action='store_true',
            help='inf debugging mode')
    parser.add_argument('--no-jit', action='store_true',
            help='disable JIT compilation')
    parser.add_argument('--x64', action='store_true',
            help='64-bit mode')
    parser.add_argument('--seed', type=int, 
            help="random seed")
    parser.add_argument('-v', '--verbose', action='store_true',
            help="verbose mode")
    parser.add_argument('--threshold', type=float, default=0.5,
            help="normalizing flow threshold")
    parser.add_argument('-e', '--exec', type=str, default='',
            help="code to evaluate before starting")
    parser.add_argument('-s', '--steps', type=int, default=100,
            help="number of optimization steps")
    parser.add_argument('-l', '--layers', type=int, default=2,
            help="number of layers in wavefunction")
    parser.add_argument('-d', '--depth', type=int, default=6,
            help="depth of normalizing flow")
    args = parser.parse_args()

    if args.debug_nan:
        jax.config.update("jax_debug_nans", True)
    if args.debug_inf:
        jax.config.update("jax_debug_infs", True)
    if args.no_jit:
        jax.config.update("jax_disable_jit", True)
    if args.x64:
        jax.config.update("jax_enable_x64", True)

    if args.seed is None:
        seed = time.time_ns()
    else:
        seed = args.seed
    key, flowKey, wfKey = jax.random.split(jax.random.PRNGKey(seed), 3)

    exec(args.exec)

    energy = energy_function(potential_ON, m2, lamda)

    flow = RealNVP(flowKey, N*D, args.depth)
    wf = DistinguishableWavefunction(wfKey, N, D, args.layers)
    #wf = BosonicWavefunction(wfKey, N, D, args.layers)

    wf_opt = optax.adam(1e-3)
    wf_opt_state = wf_opt.init(eqx.filter(wf, eqx.is_array))
    f_opt = optax.adam(1e-3)
    f_opt_state = f_opt.init(eqx.filter(flow, eqx.is_array))

    # Train
    for step in range(args.steps):
        # Train the flow.
        flow_merit = 1e10
        flow_steps = 0
        while flow_merit > args.threshold:
            flow_steps += 1
            key, xk = jax.random.split(key)
            (flow_loss_val, flow_merit), flow_loss_grad = flow_loss(flow, wf, xk)
            if args.verbose:
                print(f" FLOW {flow_merit:8.4f}     ({flow_loss_val:8.4f})")
            f_updates, f_opt_state = eqx.filter_jit(f_opt.update)(flow_loss_grad, f_opt_state)
            flow = eqx.filter_jit(eqx.apply_updates)(flow, f_updates)

        # Train the wavefunction.
        key, xk = jax.random.split(key)
        wf_loss_val, wf_loss_grad = energy(wf, flow, xk)
        wf_updates, wf_opt_state = eqx.filter_jit(wf_opt.update)(wf_loss_grad, wf_opt_state)
        wf = eqx.filter_jit(eqx.apply_updates)(wf, wf_updates)
        # Report the energy per particle.
        if args.verbose:
            print(f" E {wf_loss_val/N}")

    # Estimate the energy. (This estimate is a bit biased.)
    ests = []
    for s in range(100):
        key, xk = jax.random.split(key)
        wf_loss_val, wf_loss_grad = energy(wf, flow, xk)
        ests.append(wf_loss_val/N)
    print(*bootstrap(ests))

