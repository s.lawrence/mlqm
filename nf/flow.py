import equinox as eqx
import equinox.nn as nn
import jax
import jax.numpy as jnp

from util import *

class TrivialFlow(eqx.Module):
    """ Just scale all the variables by a single constant. Good enough for
    small systems!
    """
    scale: jnp.array

    def __init__(self, N):
        self.scale = jnp.array(1./jnp.sqrt(2.))

    def __call__(self, x):
        px = jnp.exp(-jnp.sum(x**2)/2.) / jnp.sqrt(2*jnp.pi)**len(x)
        return self.scale*x, px / self.scale**len(x)

class AffineCoupling(eqx.Module):
    mask: jnp.array
    scale: nn.MLP
    trans: nn.MLP

    def __init__(self, key, N, mask=None):
        if mask is None:
            key, key_mask = jax.random.split(key)
            self.mask = jax.random.randint(key, (N,), 0, 2)
        else:
            self.mask = mask
        key_scale, key_trans = jax.random.split(key)
        scale = nn.MLP(
                in_size=N,
                out_size=N,
                width_size=N,
                depth=1,
                key=key_scale,
                activation=jnp.tanh)
        trans = nn.MLP(in_size=N,
                       out_size=N,
                       width_size=N,
                       depth=0,
                       key=key_scale)
        self.scale = zero_mlp(scale)
        self.trans = zero_mlp(trans)

    def __call__(self, x):
        y = self.mask * x
        s = self.scale(y)
        t = self.trans(y)
        z = y + (1-self.mask) * (jnp.exp(s)*x + t)
        return z, jnp.sum((1-self.mask)*s)

class RealNVP(eqx.Module):
    layers: list

    def __init__(self, key, N, depth):
        keys = jax.random.split(key, depth)
        self.layers = [AffineCoupling(k, N) for k in keys]

    def __call__(self, x):
        lpx = -jnp.sum(x**2)/2.
        ld = 0.
        for l in self.layers:
            x, ld_ = l(x)
            ld += ld_
        return x, lpx - ld


