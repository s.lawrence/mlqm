import equinox as eqx
import jax
import jax.numpy as jnp
import numpy as np

def lu_mul(LU, x):
    N = LU.shape[0]
    L = jnp.tril(LU, k=-1)
    L = L.at[jnp.diag_indices(N)].set(1.)
    U = jnp.triu(LU)
    return L @ (U @ x)

def lu_mat(LU):
    N = LU.shape[0]
    L = jnp.tril(LU, k=-1)
    L = L.at[jnp.diag_indices(N)].set(1.)
    U = jnp.triu(LU)
    return L @ U

def ones_zeros(m, N):
    # Returns an array which is m 1s followed by (N-m) zeros.
    j = jnp.arange(N)
    return jnp.int32(j < m)

def zero_mlp(mlp):
    def where(mlp):
        return [mlp.layers[-1].weight, mlp.layers[-1].bias]
    return eqx.tree_at(where, mlp, replace_fn = jnp.zeros_like)

def bootstrap(x, w=None, N=100):
    x = np.array(x)
    if w is None:
        w = x*0 + 1
    w = np.array(w)
    y = x * w
    m = (sum(y) / sum(w))
    ms = []
    for n in range(N):
        s = np.random.choice(range(len(x)), len(x))
        ms.append((sum(y[s]) / sum(w[s])))
    ms = np.array(ms)
    return m, np.std(ms)


