import abc

import jax
import jax.numpy as jnp
import numpy as np

import mc
import wavefunction

_c = 197.32698 # hbar*c, in MeV*fm

class Model(abc.ABC):
    @abc.abstractmethod
    def wavefunction(self):
        """ Initialize and return a wavefunction.

        Args:
            key:

        Returns:
        """

    @abc.abstractmethod
    def sampler(self, key, wf, K):
        """
        Args:
            key:
            wf:
            K: batch size

        Returns:
            A function that yields samples.
        """

class OscillatorModel(Model):
    particles = None

    def __init__(self, D, c2, c4):
        self.D = D
        self.c2 = c2
        self.c4 = c4

    def potential(self, x):
        return self.c2 * jnp.sum(x**2)/2. + self.c4 * jnp.sum(x**4)/4.

    def hamiltonian(self, wf, x):
        M = 1.
        D = len(x)

        psi = wf(x)

        # Kinetic
        kin = 0.
        v = jnp.eye(D)
        for i in range(D):
            g1 = lambda y: jax.jvp(wf, (y,), (v[i],))[1]
            g2 = jax.jvp(g1, (x,), (v[i],))[1]
            kin += -psi.conj() * g2 / (2. * M)

        # Potential
        V = self.potential(x)
        pot = psi.conj() * V * psi

        return (kin + pot).real

    def wavefunction(self, key):
        return wavefunction.OneParticleWavefunction(key, self.D, 10, 2)

    def sampler(self, key, wf, N):
        return mc.GaussianMonteCarlo(key, (N,self.D), 3.)

class ManyBodyModel(Model):
    def kinetic(self, wf, x):
        psi = wf(x)

        shape = x.shape
        def wf_(y):
            return wf(y.reshape(shape))
        x = x.flatten()
        D = x.shape[0]
        M = jnp.tile(self.masses, (D,1)).transpose().flatten()

        kin = 0.
        v = jnp.eye(D)
        for i in range(D):
            g1 = lambda y: jax.jvp(wf_, (y,), (v[i],))[1]
            g2 = jax.jvp(g1, (x,), (v[i],))[1]
            kin += -psi.conj() * g2 / (2. * M[i])
        return kin.real

    def hamiltonian(self, wf, x):
        psi = wf(x)

        # Potential
        V = self.potential(x)
        pot = (psi.conj() * V * psi).real

        kin = self.kinetic(wf, x)

        return kin + pot

    @abc.abstractmethod
    def potential(self, x):
        """
        Args:
            x: A tensor of shape ``(N,D)``.
        """

class ElectricModel(ManyBodyModel):
    def __init__(self, alpha, masses, charges, species, confining=1.):
        assert len(masses) == len(charges)
        self.N = len(masses)
        self.D = 3
        self.alpha = alpha
        self.masses = np.array(masses)
        self.charges = np.array(charges)
        self.confining = confining
        self.particles = species

    def potential(self, x):
        assert x.shape == (self.N,self.D)

        # Confining potential
        Vconf = jnp.sum(0.5 * (x/self.confining)**2)

        # Electric interactions
        def pot1(n,m):
            r1 = x[n,:]
            r2 = x[m,:]
            qq = self.charges[n] * self.charges[m]
            return qq * self.alpha / jnp.linalg.norm(r1-r2)
        Velec = 0.
        for n in range(self.N):
            for m in range(n):
                Velec += pot1(n,m)

        return Vconf + Velec

class SigmaOmegaModel(ManyBodyModel):
    def __init__(self, particles, confining=20.0):
        self.D = 3
        self.N = len(particles)
        self.confining = confining
        self.particles = particles
        self.M = 939/(_c**2)
        self.masses = jnp.array(self.N * [self.M])

        # The energy, per particle, if the potential was trivial.
        self.E0 = 3 * 0.5/(self.M*self.confining**2)

    def potential(self, x):
        assert x.shape == (self.N,3)

        # Energies of the mediators, in MeV.
        Msigma = 500
        Momega = 782

        # Interaction decays, in fm.
        rsigma = _c / Msigma
        romega = _c / Momega

        # Dimensionless (relativistic) interaction strengths.
        Vsigma_dl = -2.94
        Vomega_dl = 4.4

        # Convert to MeV * fm.
        Vsigma = _c * Vsigma_dl
        Vomega = _c * Vomega_dl

        # Confining potential
        V = jnp.sum(0.5 * 1/(self.M * self.confining**2) * (x/self.confining)**2)

        # Interactions
        def pot1(n,m):
            r = jnp.linalg.norm(x[n,:]-x[m,:])
            return (Vsigma * jnp.exp(-r/rsigma) + Vomega * jnp.exp(-r/romega)) / r

        for n in range(self.N):
            for m in range(n):
                V += pot1(n,m)

        return V

    def wavefunction(self, key):
        #wf = wavefunction.DistinguishableWavefunction(key,3,self.N,width=80,depth=1)
        wf = wavefunction.RadialWavefunction(key,3,self.N,width=180,depth=0)
        cwf = wavefunction.ConfinedWavefunction(wf, self.confining)
        return cwf

    def sampler(self, key, wf):
        return mc.GaussianMonteCarlo(key, (self.N,3), self.confining/np.sqrt(2))
        #return mc.UniformMonteCarlo(key, (self.N,3), 2*self.confining)

class FermiGasModel(ManyBodyModel):
    def __init__(self, D, N, L, a, g):
        self.D = D
        self.N = N
        self.L = L
        self.a = a
        self.g = g
        self.masses = jnp.array([1.]*N)

    def potential(self, x):
        assert x.shape == (self.N,3)

        # Confining potential
        V = jnp.sum(0.5 * (x/self.L)**2)

        # Interactions.
        def pot1(n,m):
            r = jnp.linalg.norm(x[n,:]-x[m,:])
            return self.g * jnp.exp(-r)

        for n in range(self.N):
            for m in range(n):
                V += pot1(n,m)

        return V

    def wavefunction(self, key):
        wf = wavefunction.DistinguishableWavefunction(key,self.D,self.N,30,1)
        cwf = wavefunction.ConfinedWavefunction(wf, self.L)
        # TODO
        # wf = wavefunction.FermionicWavefunction(key,self.D,self.N,30,1)
        return cwf

    def sampler(self, key, wf):
        return mc.GaussianMonteCarlo(key, (self.N,3), self.L/np.sqrt(2))

