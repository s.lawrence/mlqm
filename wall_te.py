#!/usr/bin/env python

import argparse
import functools
import equinox as eqx
import jax
import jax.numpy as jnp
import optax

import model
import wavefunction

jax.config.update('jax_platform_name', 'cpu')

class WallModel(model.Model):
    particles = None

    def __init__(self, D, m, s, v):
        self.D = D
        self.m = m
        self.s = s
        self.v = v

    def potential(self, x):
        pot = self.v * (jnp.tanh(self.s*x) + 1.)/2.
        return jnp.sum(pot)

    def hamiltonian(self, wf, x):
        D = len(x)
        psi = wf(x)

        # Kinetic
        kin = 0.
        v = jnp.eye(D)
        for i in range(D):
            g1 = lambda y: jax.jvp(wf, (y,), (v[i],))[1]
            g2 = jax.jvp(g1, (x,), (v[i],))[1]
            kin += - psi.conj() * g2 / (2. * self.m)

        # Potential 
        V = self.potential(x)
        pot = psi.conj() * V * psi

        return (kin + pot).real

    def Hwf(self, wf, x):
        D = len(x)
        psi = wf(x)

        # Kinetic
        kin = 0.
        v = jnp.eye(D)
        for i in range(D):
            g1 = lambda y: jax.jvp(wf, (y,), (v[i],))[1]
            g2 = jax.jvp(g1, (x,), (v[i],))[1]
            kin += - g2 / (2. * self.m)

        # Potential 
        V = self.potential(x)
        pot = V * psi

        return kin+pot

    def wavefunction(self, key):
        return OneParticleWavefunction(key, self.D, 10, 6)

    def sampler(self, key, wf, N):
        return UniformSampler(key, (N, self.D), 15) #TODO

class OneParticleWavefunction(eqx.Module):
    """ Wavefunction of a single particle, in D dimensions.
    """
    mlp: wavefunction.MLP
    center_r: jax.Array
    center_i:jax.Array
    width_r: jax.Array
    width_i: jax.Array
    phase: jax.Array

    def __init__(self, key, N, width, depth):
        widths = [N] + [width]*depth + [2]
        self.mlp = wavefunction.MLP(key,widths)
        self.center_r = jnp.array(-0.0)  #TODO 
        self.center_i = jnp.array(-0.0)
        self.width_r = jnp.array(1.)
        self.width_i = jnp.array(1.)
        self.phase = jnp.array([0.,0.,0.,0.])

    def __call__(self, x):
        y = self.mlp(x)
        r2r = jnp.sum((x-self.center_r)**2)/self.width_r**2
        r2i = jnp.sum((x-self.center_i)**2)/self.width_i**2
        pr = jnp.exp(1j*(self.phase[0]*jnp.sum(x)+self.phase[1]*jnp.sum(x*x)))
        pi = jnp.exp(1j*(self.phase[2]*jnp.sum(x)+self.phase[3]*jnp.sum(x*x)))
        return pr * y[0] * jnp.exp(-r2r/2) + 1j * pi * y[1] * jnp.exp(-r2i/2)


class UniformSampler:
    def __init__(self, key, shape, width=1.):
        self.key = key
        self.shape = shape
        self.range = width  
        self.psamp = jnp.ones(self.shape[0])

    def __call__(self):
        self.key, key = jax.random.split(self.key)
        x = jax.random.uniform(key, self.shape, minval=-self.range, maxval=self.range)
        return x, self.psamp

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Perform time evolution", fromfile_prefix_chars="@")
    parser.add_argument('-d', '--dt', type=float, default=0.01, help="Trotter step")
    parser.add_argument('-fn', '--filename', type=str, default='wave.dat', help='Filename to output amplitude over time')
    parser.add_argument('-lr', '--learningrate', type=float, default=1e-3, help="Learning rate for time evolution")
    parser.add_argument('-i', '--inform', action='store_true', help="Inform the next time step from previous learning")
    parser.add_argument('-oi', '--optinit', action='store_true', help="Initialize optimizer at each time step")
    parser.add_argument('-s', '--nsamples', type=int, default=1<<12, help="Number of samples per training step")
    parser.add_argument('--seed', type=int, default=0,
            help="random seed")
    parser.add_argument('-T', '--time', type=float, default=1.00, help="Total time to evolve")
    parser.add_argument('-t', '--threshold', type=float, default=0.01, help="Threshold on loss function")
    parser.add_argument('-r', '--range', type=float, default=5.0, help="range of x to sample from")
    parser.add_argument('-v', '--potential', type=float, default=0.0, help="height of wall")

    args = parser.parse_args()
    print('# ' + ' '.join(f'{k}={v}' for k, v in vars(args).items()))

    # training settings
    dt = args.dt
    Ns = args.nsamples
    lr = args.learningrate

    seed = args.seed
    key = jax.random.PRNGKey(seed)
    sampleKey, wfKey = jax.random.split(jax.random.PRNGKey(seed), 2)

    # physics setting 
    model = WallModel(1, 1.0, 5.0, args.potential)
    pi = 1.0 # Initial momentum
    dx = 0.5 # Initial spread of wave packet
    xi = 0.0 # Initial position
    xrg = args.range #Sample range
    wf = model.wavefunction(wfKey)
    xf = model.sampler(sampleKey, wf, Ns)
    wf_store = model.wavefunction(wfKey)
    xf_store = model.sampler(sampleKey, wf_store, Ns)

    print(f'# pi:{pi}, dx:{dx}, xi:{xi}, xrange:{xrg}')

    # Initial wave function to prepare
    def iwf(x):
        norm = 1/jnp.sqrt(jnp.sqrt(2*jnp.pi)/dx)
        return (norm * jnp.exp(-(x-xi)**2/4./dx**2) * jnp.exp(1j*pi*(x-xi)))[0]

    @eqx.filter_jit
    @eqx.filter_value_and_grad #TODO functool.partial() didn't work.
    def loss_iwf(wf, x, psamp):
        psi = jax.vmap(wf)(x)
        psi_iwf = jax.vmap(iwf)(x)
        diff = psi - psi_iwf
        return jnp.mean((diff.conj() * diff / psamp).real) / jnp.mean(psamp)

    # Time evolution's loss function
    @eqx.filter_jit
    @functools.partial(eqx.filter_value_and_grad, has_aux=True)
    def loss_te(wfp, wfc, x, psamp):
        psi = jax.vmap(wfc)(x)
        psip = jax.vmap(wfp)(x)
        Hpsi = jax.vmap(model.Hwf, in_axes=(None,0))(wfc, x)
        Hpsip = jax.vmap(model.Hwf, in_axes=(None,0))(wfp, x)
        diff = psip/dt - psi/dt + 1j*(Hpsi + Hpsip)/2.
        num = (diff.conj() * diff / psamp).real
        den = (psi.conj() * psi / psamp).real   
        return jnp.mean(num)/jnp.mean(den), None

    # Returns a string of wavefunction at a time
    def string(time, x):
        return f'{time:.5f} ' + ' '.join([str('{:.5f}'.format(i)) for i in x]) + '\n'


    opt_g = optax.adam(1e-3)
    opt_g_state = opt_g.init(wf)

    print('#Ground state preparation')
    loss = 10
    try:
        while loss > 1e-6:  #TODO    
            x, psamp = xf()
            loss, loss_grad = loss_iwf(wf, x, psamp)
            updates, opt_g_state = jax.jit(opt_g.update)(loss_grad, opt_g_state)      
            wf = eqx.filter_jit(eqx.apply_updates)(wf, updates)
            #print(f'{loss:.6f} {wf.width_r:.5f} {wf.width_i:.5f} {wf.center_r:.5f} {wf.center_i:.5f}')
    except KeyboardInterrupt:
        pass

    rrange = xrg  #TODO
    rN = 10000
    dx = 2*rrange/float(rN)
    xuni = jnp.linspace(-rrange,rrange,rN).reshape((rN,1))
    def norm(wf):
        psi = jax.vmap(wf)(xuni)
        nor = jnp.sum((psi.conj()*psi*dx).real)
        return nor

    mrange = xrg  #TODO
    mN = int(2*mrange/0.01)
    xms = jnp.linspace(-mrange,mrange,mN).reshape((mN,1))
    def wave(wf):
        psi = jax.vmap(wf)(xms)
        nor = norm(wf)
        amp = (psi.conj()*psi).real
        return (psi/jnp.sqrt(nor)).reshape(mN,), (amp/nor).reshape(mN,)

    f =  open(args.filename, 'w')
    f.write('# ' + ' '.join(f'{k}={v}' for k, v in vars(args).items()) + '\n')
    f.write(f'# pi:{pi}, dx:{dx}, xi:{xi}, xrange:{xrg} \n')

    psi, amp = wave(wf)
    f.write(string(0, xms.reshape(mN,)))
    f.write(string(0, psi.real))
    f.write(string(0, psi.imag))

    print('#Starting time evolution')
    wf_store = wf
    time = 0
    opt_te = optax.adam(lr)
    opt_te_state = opt_te.init(wf)
    wf_change = jax.tree_util.tree_map(lambda x, y: x - y, wf, wf_store)

    try:
        while time < args.time + dt:
            # Inform next wf from last time step
            if args.inform:
                wf = jax.tree_util.tree_map(lambda x, y: x + y, wf, wf_change)
            x, psamp = xf_store()
            (loss_val_init, _), loss_grad = loss_te(wf, wf_store, x, psamp)

            if args.optinit:
                opt_te_state = opt_te.init(wf)
            time = time + dt
            loss_val = 100
            counter = 0
            while loss_val > args.threshold:
                x, psamp = xf_store()
                (loss_val, _), loss_grad = loss_te(wf, wf_store, x, psamp)
                if loss_val > args.threshold:
                    updates, opt_te_state = jax.jit(opt_te.update)(loss_grad, opt_te_state)
                    wf = eqx.filter_jit(eqx.apply_updates)(wf, updates)
                #print(f'{time:.4f} {loss_val:.6f} {wf.width_r:.5f} {wf.width_i:.5f} {wf.center_r:.5f} {wf.center_i:.5f}')
                counter += 1

            wf_change = jax.tree_util.tree_map(lambda x, y: x - y, wf, wf_store)
            wf_store = wf

            # The average wf parameter and their change
            ave_p = jnp.mean(jnp.abs(jax.flatten_util.ravel_pytree(wf)[0]))
            ave_c = jnp.mean(jnp.abs(jax.flatten_util.ravel_pytree(wf_change)[0]))

            print(f'{time:.4f} {loss_val:.5f} {ave_p:.5f} {ave_c:.5f} {counter} {loss_val_init:.5f} {wf.phase[0]:.5f} {wf.phase[1]:.5f} {wf.phase[3]:.5f} {wf.phase[3]:.5f}')
            psi, amp = wave(wf_store)
            f.write(string(time, psi.real))
            f.write(string(time, psi.imag))

    except KeyboardInterrupt:
        pass

    f.close()
    
#    amp_store = jnp.array(amp_store)
#    with open(args.filename, 'w') as f:
#        for i in range(len(amp_store[0,:])):
#            pr = ' '.join([str('{:.5f}'.format(i)) for i in amp_store[:,i]])
#           f.write(pr + '\n')


        
        


