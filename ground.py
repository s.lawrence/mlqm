#!/usr/bin/env python

"""
Variational training of a ground state.
"""

import argparse
import functools
import os
import pickle
import sys

import equinox as eqx
import jax
import jax.numpy as jnp
import numpy as np
import optax
jax.config.update('jax_platform_name', 'cpu')

import wavefunction

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
            description="Train ground state",
            formatter_class=argparse.ArgumentDefaultsHelpFormatter,
            fromfile_prefix_chars="@")
    parser.add_argument('model', type=str, help='model directory')
    parser.add_argument('-B', '--batch', type=int, default=10, help='batch size (log 2)')
    parser.add_argument('-K', '--batches', type=int, default=1, help='batches to use')
    parser.add_argument('--seed', type=int, default=0,
            help="random seed")
    parser.add_argument('--seed-time', action='store_true',
            help="seed PRNG with current time")
    parser.add_argument('-i', '--init', action='store_true',
            help="re-initialize wavefunction")

    gdargs = parser.add_mutually_exclusive_group()
    gdargs.add_argument('--adam', type=float, metavar=('rate'), default=1e-3,
            help='Use ADAM')
    gdargs.add_argument('--sgd', type=float, metavar=('rate'), default=1e-3,
            help='Use SGD')
    gdargs.add_argument('--yogi', type=float, metavar=('rate'), default=1e-3,
            help='Use YOGI')

    args = parser.parse_args()

    seed = args.seed
    if args.seed_time:
        seed = time.time_ns()
    sampleKey, wfKey = jax.random.split(jax.random.PRNGKey(seed), 2)

    def probability(wf, x):
        psi = wf(x)
        return jnp.abs(psi)**2

    modelFilename = os.path.join(args.model, 'model.pickle')
    wfEqxFilename = os.path.join(args.model, 'wf.eqx')
    with open(modelFilename, 'rb') as f:
        model = pickle.load(f)
    wf = model.wavefunction(wfKey)
    if not args.init:
        try:
            wf = eqx.tree_deserialise_leaves(wfEqxFilename, wf)
        except FileNotFoundError:
            pass
    xf = model.sampler(sampleKey, wf)

    def save():
        eqx.tree_serialise_leaves(wfEqxFilename, wf)

    def energy(wf, x, psamp):
        p = jax.vmap(lambda y: jnp.abs(wf(y))**2)(x)
        h = jax.vmap(model.hamiltonian, in_axes=(None,0))(wf, x)

        num = h/psamp
        den = p/psamp
        return jnp.mean(num), jnp.mean(den)

    @eqx.filter_jit
    @functools.partial(eqx.filter_value_and_grad, has_aux=True)
    def loss(wf, x, psamp):
        num, den = energy(wf, x, psamp)
        return num/den, None

    if args.adam is not None:
        opt = optax.adam(args.adam)
    elif args.sgd is not None:
        opt = optax.sgd(args.sgd)
    elif args.yogi is not None:
        opt = optax.yogi(args.yogi)
    else:
        opt = optax.adam(1e-3)
    opt_state = opt.init(wf)
    try:
        while True:
            # TODO jackknife
            # TODO repeat `args.batches` times
            x, psamp = xf(1<<args.batch)
            (loss_val, loss_err), loss_grad = loss(wf, x, psamp)
            updates, opt_state = jax.jit(opt.update)(loss_grad, opt_state)
            wf = eqx.filter_jit(eqx.apply_updates)(wf, updates)
            print(loss_val - 2*model.E0, loss_err)
            save()
    except KeyboardInterrupt:
        pass
    save()

