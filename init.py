#!/usr/bin/env python

import argparse
import os
import pickle
import shutil
import sys

import equinox as eqx
import jax
jax.config.update('jax_platform_name', 'cpu')

import model
import wavefunction

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
            description="Initialize model",
            fromfile_prefix_chars="@")
    parser.add_argument('model', type=str, help='model directory')
    parser.add_argument('-f', '--force', action='store_true', help='overwrite')
    parser.add_argument('--seed', type=int, default=0,
            help="random seed")
    parser.add_argument('--seed-time', action='store_true',
            help="seed PRNG with current time")

    subparsers = parser.add_subparsers(required=True)

    def mkoscillator(args):
        return model.OscillatorModel(args.D, args.c2, args.c4)

    parser_oscillator = subparsers.add_parser('oscillator')
    parser_oscillator.add_argument('D', type=int, help='dimensions')
    parser_oscillator.add_argument('c2', type=float, help='quadratic coupling')
    parser_oscillator.add_argument('c4', type=float, help='quartic coupling')
    parser_oscillator.set_defaults(func=mkoscillator)

    def mkelectric(args):
        return model.ElectricModel(1/137, [1,1],[1,-1],['a','a'])

    parser_electric = subparsers.add_parser('electric')
    parser_electric.set_defaults(func=mkelectric)

    def mksigma_omega(args):
        return model.SigmaOmegaModel(['Pu','Nu'])

    parser_sigma_omega = subparsers.add_parser('sigma_omega')
    parser_sigma_omega.set_defaults(func=mksigma_omega)

    def mkFermiGas(args):
        return model.FermiGasModel(args.D, args.N, args.L, args.a, args.g)

    parserFermiGas = subparsers.add_parser('FermiGas')
    parserFermiGas.add_argument('D', type=int, help='dimensions')
    parserFermiGas.add_argument('N', type=int, help='number of particles')
    parserFermiGas.add_argument('L', type=float, help='size of well')
    parserFermiGas.add_argument('a', type=float, help='interaction range')
    parserFermiGas.add_argument('g', type=float, help='interaction strength')
    parserFermiGas.set_defaults(func=mkFermiGas)

    args = parser.parse_args()

    seed = args.seed
    if args.seed_time:
        seed = time.time_ns()
    _, wfKey = jax.random.split(jax.random.PRNGKey(seed), 2)

    model = args.func(args)
    wf = model.wavefunction(wfKey)

    # Make the directory. Two files need to be created inside: the model
    # description, and an initial wavefunction.
    if args.force:
        try:
            shutil.rmtree(args.model)
        except:
            pass
    try:
        os.makedirs(args.model)
    except FileExistsError:
        print('Directory already exists! Use -f to overwrite.')
        sys.exit(1)
    modelFilename = os.path.join(args.model, 'model.pickle')
    wfEqxFilename = os.path.join(args.model, 'wf.eqx')

    with open(modelFilename, 'wb') as f:
        pickle.dump(model, f)

    eqx.tree_serialise_leaves(wfEqxFilename, wf)

