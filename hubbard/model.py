from abc import ABC,abstractmethod
from dataclasses import dataclass
from typing import Callable

from wavefunction import *

class Geometry(ABC):
    @abstractmethod
    def sites(self):
        pass

    @abstractmethod
    def foreach_neighbor(self, i, func):
        pass

    @abstractmethod
    def coordinates(self, i):
        pass

    def _check_symmetry(self):
        for i in self.sites():
            for j in self.sites():
                ni, nj = False, False
                # TODO

@dataclass
class LineGeometry(Geometry):
    L: int

    def sites(self):
        return jnp.arange(self.L)

    def foreach_neighbor(self, i, func):
        func((i+1)%self.L)
        func((i-1)%self.L)

    def coordinates(self, i):
        return jnp.array([i, 0, 0])

@dataclass
class Hubbard:
    geometry: Geometry
    Nu: int
    Nd: int

    t: float
    U: float

    def __post_init__(self):
        self.V = len(self.geometry.sites())

    def wavefunction(self, key):
        return Wavefunction(key, self.Nu, self.Nd)

    def hamiltonian(self, wf, xu, xd):
        psi = wf(xu,xd)

        # Hopping.
        hopping = 0.j
        us, ds = jnp.arange(self.Nu), jnp.arange(self.Nd)
        for i in us:
            def hop(y):
                nonlocal hopping
                yu = xu.at[i].set(y)
                hopping += wf(yu,xd)
            self.geometry.foreach_neighbor(xu[i], hop)
        for i in ds:
            def hop(y):
                nonlocal hopping
                yd = xd.at[i].set(y)
                hopping += wf(xu,yd)
            self.geometry.foreach_neighbor(xd[i], hop)

        # Interaction
        occupied_u = jnp.zeros(self.V)
        occupied_u = occupied_u.at[xu].set(1)
        occupied_d = jnp.zeros(self.V)
        occupied_d = occupied_d.at[xd].set(1)
        interaction = jnp.sum(occupied_u*occupied_d)
        return -self.t*hopping + self.U*interaction*psi

