#!/usr/bin/env python

import argparse
import os
import pickle
import sys

import numpy as np

from model import *

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
            description="Exact diagonalization of Hubbard model",
            formatter_class=argparse.ArgumentDefaultsHelpFormatter,
            fromfile_prefix_chars="@")
    parser.add_argument('model', type=str, help='model directory')
    args = parser.parse_args()

    modelFilename = os.path.join(args.model, 'model.pickle')
    with open(modelFilename, 'rb') as f:
        model = pickle.load(f)

    ######################################
    # Creation and annihilation operators

    pauli_1 = np.eye(2,dtype=np.complex128)+0j
    pauli_x = np.array([[0,1],[1,0j]],dtype=np.complex128)
    pauli_y = np.array([[0,-1j],[1j,0]],dtype=np.complex128)
    pauli_z = np.array([[1,0],[0j,-1]],dtype=np.complex128)
    pauli_p = (pauli_x + 1j * pauli_y)/2
    pauli_m = (pauli_x - 1j * pauli_y)/2
    def adj(M):
        return M.conj().T

    sites = model.geometry.sites()
    D = 2**(2*len(sites))
    c_p = [np.eye(1,dtype=np.complex128)+0j]*len(sites)
    c_m = [np.eye(1,dtype=np.complex128)+0j]*len(sites)
    for i in sites:
        for j in sites:
            if i < j:
                c_p[i] = np.kron(pauli_z, c_p[i])
                c_p[i] = np.kron(pauli_z, c_p[i])
                c_m[i] = np.kron(pauli_z, c_m[i])
                c_m[i] = np.kron(pauli_z, c_m[i])
            elif i == j:
                c_p[i] = np.kron(pauli_z, c_p[i])
                c_p[i] = np.kron(pauli_p, c_p[i])
                c_m[i] = np.kron(pauli_m, c_m[i])
                c_m[i] = np.kron(pauli_1, c_m[i])
            else:
                c_p[i] = np.kron(pauli_1, c_p[i])
                c_p[i] = np.kron(pauli_1, c_p[i])
                c_m[i] = np.kron(pauli_1, c_m[i])
                c_m[i] = np.kron(pauli_1, c_m[i])

    n_p = [adj(M) @ M for M in c_p]
    n_m = [adj(M) @ M for M in c_m]

    ##############
    # Hamiltonian

    I = np.eye(D,dtype=np.complex128)
    H = np.zeros((D,D),dtype=np.complex128)
    N = np.zeros((D,D),dtype=np.complex128)
    N_p = np.zeros((D,D),dtype=np.complex128)
    N_m = np.zeros((D,D),dtype=np.complex128)

    for i in sites:
        def add_hop(j):
            global H
            H += -model.t * (adj(c_p[i]) @ c_p[j] + adj(c_m[i]) @ c_m[j])
        model.geometry.foreach_neighbor(i, add_hop)
        H += model.U * n_p[i] @ n_m[i]
        N_p += n_p[i]
        N_m += n_m[i]

    # Create projectors
    Pp = np.diag(np.diag(N_p) == model.Nu).astype(np.complex128)
    Pm = np.diag(np.diag(N_m) == model.Nd).astype(np.complex128)
    P = Pp @ Pm
    print(np.linalg.eigh(H@P)[0][0])

    sys.exit(0)

t = 1.
U = float(sys.argv[1])
mu_p = -1e-3
mu_m = np.exp(0)*1e-3      # The ratio must not be rational.

# TODO find a cleaner way to do simultaneous diagonalization

###################
# Lattice geometry

if False:
    SITES = 3
    LINKS = [
            (0,1),
            (1,2),
            (0,2)
            ]
if False:
    SITES = 2
    LINKS = [
            (0,1)
            ]

if True:
    SITES = 4
    LINKS = [
            (0,1),
            (1,2),
            (2,3),
            (3,0),
            ]

D = 2**(2*SITES)

######################################
# Creation and annihilation operators

pauli_1 = np.eye(2,dtype=np.complex128)+0j
pauli_x = np.array([[0,1],[1,0j]],dtype=np.complex128)
pauli_y = np.array([[0,-1j],[1j,0]],dtype=np.complex128)
pauli_z = np.array([[1,0],[0j,-1]],dtype=np.complex128)
pauli_p = (pauli_x + 1j * pauli_y)/2
pauli_m = (pauli_x - 1j * pauli_y)/2
def adj(M):
    return M.conj().T

c_p = [np.eye(1,dtype=np.complex128)+0j]*SITES
c_m = [np.eye(1,dtype=np.complex128)+0j]*SITES
for i in range(SITES):
    for j in range(SITES):
        if i < j:
            c_p[i] = np.kron(pauli_z, c_p[i])
            c_p[i] = np.kron(pauli_z, c_p[i])
            c_m[i] = np.kron(pauli_z, c_m[i])
            c_m[i] = np.kron(pauli_z, c_m[i])
        elif i == j:
            c_p[i] = np.kron(pauli_z, c_p[i])
            c_p[i] = np.kron(pauli_p, c_p[i])
            c_m[i] = np.kron(pauli_m, c_m[i])
            c_m[i] = np.kron(pauli_1, c_m[i])
        else:
            c_p[i] = np.kron(pauli_1, c_p[i])
            c_p[i] = np.kron(pauli_1, c_p[i])
            c_m[i] = np.kron(pauli_1, c_m[i])
            c_m[i] = np.kron(pauli_1, c_m[i])

n_p = [adj(M) @ M for M in c_p]
n_m = [adj(M) @ M for M in c_m]

##############
# Hamiltonian

I = np.eye(D,dtype=np.complex128)
H = np.zeros((D,D),dtype=np.complex128)
N = np.zeros((D,D),dtype=np.complex128)
N_p = np.zeros((D,D),dtype=np.complex128)
N_m = np.zeros((D,D),dtype=np.complex128)
for (i,j) in LINKS:
    H += -t * (adj(c_p[i]) @ c_p[j] + adj(c_m[i]) @ c_m[j])
    H += -t * (adj(c_p[j]) @ c_p[i] + adj(c_m[j]) @ c_m[i])
for i in range(SITES):
    #H += U * (n_p[i]-I/2) @ (n_m[i]-I/2) - U*I/4
    H += U * n_p[i] @ n_m[i]
    H += -mu_p * n_p[i] - mu_m * n_m[i]
    N_p += n_p[i]
    N_m += n_m[i]

N = N_p + N_m

##############
# Eigenstates

Hvals, Hvecs = np.linalg.eigh(H)

if False:
    for i in range(D):
        print(Hvals[i], round((adj(Hvecs[:,i]) @ N @ Hvecs[:,i]).real,2))
elif False:
    Ns = []
    for i in range(D):
        Ns.append(round((adj(Hvecs[:,i]) @ N @ Hvecs[:,i]).real,2))
    Es = {}
    for i in range(D):
        if Ns[i] not in Es:
            Es[Ns[i]] = Hvals[i]
    for num in sorted(list(Es)):
        print(num, Es[num])
elif True:
    N_ps, N_ms = [], []
    for i in range(D):
        v = Hvecs[:,i]
        Np = adj(v) @ N_p @ v
        fail = np.sum(np.abs(Np * v - N_p @ v))
        print(fail, adj(v) @ N_p @ v, adj(v) @ H @ v)
        N_ps.append(round((adj(v) @ N_p @ v).real,2))
        N_ms.append(round((adj(v) @ N_m @ v).real,2))
    Es = {}
    for i in range(D):
        Ns = (N_ps[i], N_ms[i])
        if Ns not in Es:
            Es[Ns] = Hvals[i]
    for Ns in sorted(list(Es)):
        print(Ns, Es[Ns])
else:
    print(Hvals[0], round((adj(Hvecs[:,0]) @ N @ Hvecs[:,0]).real,2))

