from typing import Callable,List

import equinox as eqx
import jax
import jax.numpy as jnp

class Slater(eqx.Module):
    mlp: eqx.nn.MLP
    N: int

    def __init__(self, key, Nu, Nd):
        self.N = Nu + Nd
        IN = 1
        OUT = Nu+Nd
        WIDTH = 4
        DEPTH = 2
        self.mlp = eqx.nn.MLP(IN, OUT, WIDTH, DEPTH, key=key)

    def __call__(self, xu, xd):
        x = jnp.concatenate([xu, xd])
        x = x.reshape((self.N, 1))
        mat = jax.vmap(self.mlp)(x)
        return jnp.linalg.det(mat)

class MultiSlater(eqx.Module):
    slaters: List[Slater]

class Temp(eqx.Module):
    mlp: eqx.nn.MLP

    def __init__(self, key, Nu, Nd):
        IN = OUT = Nu+Nd
        WIDTH = IN
        DEPTH = 2
        self.mlp = eqx.nn.MLP(IN, OUT, WIDTH, DEPTH, key=key)

    def __call__(self, xu, xd):
        # TODO antisymmetrize
        return self.mlp(jnp.concatenate([xu,xd]))

Wavefunction = Slater
