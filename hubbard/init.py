#!/usr/bin/env python

import argparse
import os
import pickle
import shutil
import sys

import equinox as eqx
import jax
jax.config.update('jax_platform_name', 'cpu')

from model import *
from wavefunction import *

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
            description="Initialize model",
            fromfile_prefix_chars="@")
    parser.add_argument('model', type=str, help='model directory')
    parser.add_argument('-f', '--force', action='store_true', help='overwrite')
    parser.add_argument('--seed', type=int, default=0,
            help="random seed")
    parser.add_argument('--seed-time', action='store_true',
            help="seed PRNG with current time")

    parser.add_argument('geometry', type=str, help='model geometry')
    parser.add_argument('L', type=int, help='lattice size')
    parser.add_argument('t', type=float)
    parser.add_argument('U', type=float)
    parser.add_argument('Nu', type=int)
    parser.add_argument('Nd', type=int)

    args = parser.parse_args()

    seed = args.seed
    if args.seed_time:
        seed = time.time_ns()
    _, wfKey = jax.random.split(jax.random.PRNGKey(seed), 2)

    geometries = {
            'line': LineGeometry,
        }

    model = Hubbard(geometries[args.geometry](args.L), args.Nu, args.Nd, args.t, args.U)
    wf = model.wavefunction(wfKey)

    # Make the directory. Two files need to be created inside: the model
    # description, and an initial wavefunction.
    if args.force:
        try:
            shutil.rmtree(args.model)
        except:
            pass
    try:
        os.makedirs(args.model)
    except FileExistsError:
        print('Directory already exists! Use -f to overwrite.')
        sys.exit(1)
    modelFilename = os.path.join(args.model, 'model.pickle')
    wfEqxFilename = os.path.join(args.model, 'wf.eqx')

    with open(modelFilename, 'wb') as f:
        pickle.dump(model, f)

    eqx.tree_serialise_leaves(wfEqxFilename, wf)

