#!/usr/bin/env python

import argparse
import os
import pickle
import sys

import equinox
from equinox import nn
import jax
import jax.numpy as jnp
import numpy as np
import optax
jax.config.update('jax_platform_name', 'cpu')

from model import *
from wavefunction import *

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
            description="Train Hubbard model ground state",
            formatter_class=argparse.ArgumentDefaultsHelpFormatter,
            fromfile_prefix_chars="@")
    parser.add_argument('model', type=str, help='model directory')
    parser.add_argument('--seed', type=int, default=0,
            help="random seed")
    parser.add_argument('--seed-time', action='store_true',
            help="seed PRNG with current time")
    parser.add_argument('-i', '--init', action='store_true',
            help="re-initialize wavefunction")
    args = parser.parse_args()

    seed = args.seed
    if args.seed_time:
        seed = time.time_ns()
    sampleKey, wfKey = jax.random.split(jax.random.PRNGKey(seed), 2)

    modelFilename = os.path.join(args.model, 'model.pickle')
    wfEqxFilename = os.path.join(args.model, 'wf.eqx')
    with open(modelFilename, 'rb') as f:
        model = pickle.load(f)
    wf = model.wavefunction(wfKey)
    if not args.init:
        try:
            wf = eqx.tree_deserialise_leaves(wfEqxFilename, wf)
        except FileNotFoundError:
            pass

    def save():
        eqx.tree_serialise_leaves(wfEqxFilename, wf)

    @eqx.filter_jit
    @eqx.filter_value_and_grad
    def loss(wf, key):
        # TODO feed coords(x) into wf, instead of just x
        K = 1 << 15
        # Sample a bunch of positions.
        keyu, keyd = jax.random.split(key)
        xu = jax.random.randint(keyu, (K, model.Nu), 0, model.V)
        xd = jax.random.randint(keyd, (K, model.Nd), 0, model.V)
        # Compute wavefunction, and Hamiltonian times wavefunction.
        psi = jax.vmap(wf, in_axes=(0,0))(xu, xd)
        Hpsi = jax.vmap(lambda yu,yd: model.hamiltonian(wf, yu, yd))(xu, xd)
        # Contributions to numerator and normalization.
        ham = (psi.conj() * Hpsi).real
        den = abs(psi)**2
        return jnp.sum(ham)/jnp.sum(den)

    opt = optax.adam(3e-4)
    opt_state = opt.init(eqx.filter(wf, eqx.is_array))
    try:
        step = 0
        while True:
            step += 1
            skey, sampleKey = jax.random.split(sampleKey)
            loss_val, loss_grad = loss(wf, skey)
            updates, opt_state = jax.jit(opt.update)(loss_grad, opt_state)
            wf = eqx.filter_jit(eqx.apply_updates)(wf, updates)
            print(loss_val, step)
            save()
    except KeyboardInterrupt:
        pass
    save()

