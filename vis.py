#!/usr/bin/env python

import argparse
import pickle

import equinox as eqx
import jax
import jax.numpy as jnp
import matplotlib.pyplot as plt
import numpy as np

from wavefunction import *

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Visualize state", fromfile_prefix_chars="@")
    parser.add_argument('wavefunction', type=str, help='wavefunction filename')
    args = parser.parse_args()

    with open(args.wavefunction, 'rb') as f:
        model = pickle.load(f)

    if isinstance(model, OneParticleWavefunction):
        x = jnp.linspace(-3.,3.,10000)[:,jnp.newaxis]
        wf = jax.vmap(model)(x)
        plt.plot(x, np.abs(wf)**2)
        plt.show()
    else:
        N, D = model.N, model.D
        x = np.random.uniform(size=(1<<14,N,D), low=-5, high=5)
        wf = jax.vmap(model)(x)
        plt.figure()
        plt.scatter(x[:,0,0], np.abs(wf)**2)
        plt.show()

