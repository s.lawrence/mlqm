#!/usr/bin/env python

# Calculating the deuteron binding energy in a sigma-omega model.

from functools import partial

import jax
import jax.numpy as jnp
import numpy as np
from scipy.optimize import minimize

jax.config.update('jax_platform_name', 'cpu')

_c = 197.32698 # hbar*c, in MeV*fm

#@partial(jnp.vectorize, signature='(i)->()')
def V(x):
    assert x.shape == (3,)

    # Energies of the mediators, in MeV.
    Msigma = 500
    Momega = 782

    # Interaction decays, in fm.
    rsigma = _c / Msigma
    romega = _c / Momega

    # Dimensionless (relativistic) interaction strengths.
    Vsigma_dl = -2.94
    Vomega_dl = 4.4

    # Convert to MeV * fm.
    Vsigma = _c * Vsigma_dl
    Vomega = _c * Vomega_dl

    # Interactions
    r = jnp.linalg.norm(x)
    return (Vsigma * jnp.exp(-r/rsigma) + Vomega * jnp.exp(-r/romega)) / r

@jax.jit
@partial(jnp.vectorize, signature='(i),(j)->(),()')
def ansatz(x,p):
    M = 939/(_c**2)
    R = p[0]

    r = jnp.linalg.norm(x)

    def wf(r):
        return jnp.exp(-(r/R)**2 / 2.) * (1. + p[1]*r**1 + p[2]*r**2 + p[3]*r**3 + p[4]*r**4 + p[5]*r**5 + p[6]*r**6)

    def dr_wf(r):
        return jax.grad(wf)(r)

    def d2r_wf(r):
        return jax.grad(jax.grad(wf))(r)

    psi = wf(r)
    dr_psi = dr_wf(r)
    d2r_psi = d2r_wf(r)

    lap_psi = 2/r * dr_psi + d2r_psi

    V_psi = V(x)*psi

    return psi, V_psi - lap_psi/(2*M)

K = 10000000
x0 = jnp.array(np.random.normal(size=(K,3)))
psamp = jnp.exp(-jnp.sum(x0*x0, axis=1)/2.)

def energy(p):
    x = p[0]*x0
    p = jnp.array(p)
    psi, Hpsi = ansatz(x,p)
    return np.sum(psi.conj()*Hpsi/psamp)/np.sum(psi.conj()*psi/psamp)

p0 = np.array([1.8, 0., -.07, 0., 0.006, 0., 0.])
print(energy(p0))

if False:
    p0 = np.array([1.8, 0., -.07, 0., 0.006, 0., 0.])
    print(energy(p0))
    print(minimize(energy, p0))

if False:
    R0 = 1.8
    x = R0*x0

    for a1 in np.linspace(-0.1,0.1,101):
        p0 = jnp.array([R0,-.04,-.07,0,.006,0,0])
        print(a1, energy(p0))

if False:
    for R0 in np.linspace(1.4,2.0,20):
        p0 = np.array([R0, -.07, 0.006, 0.])
        print(R0, energy(p0))

