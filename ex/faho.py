#!/usr/bin/env python

import functools
import sys
from typing import Callable

import numpy as np

import equinox as eqx
from equinox import nn
import jax
import jax.numpy as jnp
import optax
jax.config.update('jax_platform_name', 'cpu')

DIM = 3
N = 3

class MLP(eqx.Module):
    activation: Callable = staticmethod(jax.nn.celu)
    layers: list

    def __init__(self, key, width):
        Nlayers = len(width)-1
        keys = jax.random.split(key, Nlayers)
        self.layers = [nn.Linear(width[n], width[n+1], key=keys[n]) for n in range(Nlayers)]

    def __call__(self, x):
        act = self.activation
        for layer in self.layers[:-1]:
            x = act(layer(x))
        return self.layers[-1](x)

class OddMLP(eqx.Module):
    """
    A multi-layer perceptron with the guarantee that f(x) = -f(-x).
    """
    activation: Callable = staticmethod(jax.nn.tanh)
    layers: list

    def __init__(self, key, width):
        Nlayers = len(width)-1
        keys = jax.random.split(key, Nlayers)
        self.layers = [nn.Linear(width[n], width[n+1], key=keys[n], use_bias=False) for n in range(Nlayers)]

    def __call__(self, x):
        act = self.activation
        for layer in self.layers[:-1]:
            x = act(layer(x))
        return self.layers[-1](x)

class Wavefunction(eqx.Module):
    N: int
    dets: jnp.array
    coords: MLP
    final: OddMLP

    def __init__(self, key, N):
        self.N = N
        coords_key, det_key = jax.random.split(key)

        # Nonlinear coordinates
        #K = 2*N + 2
        K = 40
        self.coords = MLP(coords_key,[DIM]+[K]*2)
        #self.coords = nn.Linear(DIM,K,key=coords_key)

        dets = []
        for key in jax.random.split(det_key, DIM*N):
            dets.append(jax.random.choice(key, K, shape=(N,), replace=False))
        self.dets = jnp.array(dets)

        self.final = OddMLP(key, [len(dets)]*2+[2])

    def __call__(self, x):
        window = jnp.exp(-jnp.sum(x**2)/2.)
        # Get coordinates.
        y = jax.vmap(self.coords)(x)
        # Take DIM*N determinants.
        def det(indices):
            w = y[:,indices]
            return jnp.linalg.det(w)
        z = jax.vmap(det)(self.dets)
        wf = self.final(z)
        return (wf[0] + 1j*wf[1])*window

def kinetic(wf, x):
    psi = wf(x)

    shape = x.shape
    def wf_(y):
        return wf(y.reshape(shape))
    def wf_r(y):
        return wf(y.reshape(shape)).real
    def wf_i(y):
        return wf(y.reshape(shape)).imag
    x = x.flatten()
    D = x.shape[0]
    M = jnp.tile(jnp.array([1.,1.]), (D,1)).transpose().flatten()

    v = jnp.eye(D)

    def kin_i(i):
        g1 = lambda y: jax.jvp(wf_, (y,), (v[i],))[1]
        g2 = jax.jvp(g1, (x,), (v[i],))[1]
        return -psi.conj() * g2 / (2. * M[i])
    kin = jnp.sum(jax.vmap(kin_i)(jnp.arange(D)))
    return kin.real

def potential(x):
    # No interaction, just a harmonic oscillator.
    return jnp.sum(0.5 * (x/1.)**2)

def hamiltonian(wf, x):
    psi = wf(x)

    # Potential
    V = potential(x)
    pot = (psi.conj() * V * psi).real

    kin = kinetic(wf, x)

    return kin + pot

@eqx.filter_jit
@eqx.filter_value_and_grad
def loss(wf, x, psamp):
    p = jax.vmap(lambda y: jnp.abs(wf(y))**2)(x)
    h = jax.vmap(lambda y: hamiltonian(wf,y).real)(x)

    num = h/psamp
    den = p/psamp
    return jnp.sum(num)/jnp.sum(den)

def xf(K):
    x = np.random.normal(size=(K,N,DIM))/np.sqrt(2)
    return jnp.array(x), jnp.array(jnp.exp(-jnp.sum(x**2.,axis=(1,2))))

key = jax.random.PRNGKey(0)
wf = Wavefunction(key, N)

opt = optax.adam(1e-4)
opt_state = opt.init(wf)
while True:
    x, psamp = xf(1 << 10)
    loss_val, loss_grad = loss(wf, x, psamp)
    print(loss_val)
    updates, opt_state = jax.jit(opt.update)(loss_grad, opt_state)
    wf = eqx.filter_jit(eqx.apply_updates)(wf, updates)

