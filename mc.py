import jax
import jax.numpy as jnp
import numpy as np

class GaussianMonteCarlo:
    def __init__(self, key, shape, dev=1.):
        self.key = key
        self.shape = shape
        self.dev = dev

    def __call__(self, K=1):
        self.key, key = jax.random.split(self.key)
        x = self.dev * jax.random.normal(key, (K,)+self.shape)
        def prob(y):
            return jnp.exp(-jnp.sum((y/self.dev)**2)/2.)
        return x, jax.vmap(prob)(x)

class UniformMonteCarlo:
    def __init__(self, key, shape, spread=1.):
        self.key = key
        self.shape = shape
        self.spread = spread

    def __call__(self, K):
        self.key, key = jax.random.split(self.key)
        x = jax.random.uniform(key, (K,)+self.shape, minval=-self.spread, maxval=self.spread)
        return x, jnp.ones(K)

class Metropolis:
    def __init__(self, key, shape):
        self.key = key
        self.shape = shape

    def step(self):
        pass

    def __call__(self):
        pass

