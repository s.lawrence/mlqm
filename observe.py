#!/usr/bin/env python

import argparse
import functools
import os
import pickle
import sys

import equinox as eqx
import jax
import jax.numpy as jnp
import numpy as np
import optax
jax.config.update('jax_platform_name', 'cpu')

import wavefunction

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
            description="Observe",
            formatter_class=argparse.ArgumentDefaultsHelpFormatter,
            fromfile_prefix_chars="@")
    parser.add_argument('model', type=str, help='model directory')
    parser.add_argument('-B', '--batch', type=int, default=10, help='batch size (log 2)')
    parser.add_argument('--seed', type=int, default=0,
            help="random seed")
    parser.add_argument('--seed-time', action='store_true',
            help="seed PRNG with current time")
    args = parser.parse_args()

    seed = args.seed
    if args.seed_time:
        seed = time.time_ns()
    sampleKey, wfKey = jax.random.split(jax.random.PRNGKey(seed), 2)

    modelFilename = os.path.join(args.model, 'model.pickle')
    wfEqxFilename = os.path.join(args.model, 'wf.eqx')
    with open(modelFilename, 'rb') as f:
        model = pickle.load(f)
    wf = model.wavefunction(wfKey)
    wf = eqx.tree_deserialise_leaves(wfEqxFilename, wf)
    xf = model.sampler(sampleKey, wf)

    @eqx.filter_jit
    def energy(wf, x, psamp):
        p = jax.vmap(lambda y: jnp.abs(wf(y))**2)(x)
        h = jax.vmap(model.hamiltonian, in_axes=(None,0))(wf, x)

        num = h/psamp
        den = p/psamp
        return jnp.mean(num), jnp.mean(den)

    try:
        nums = np.array([])
        dens = np.array([])
        while True:
            x, psamp = xf(1<<args.batch)
            num, den = energy(wf, x, psamp)
            nums = np.append(nums, num)
            dens = np.append(dens, den)
            print(np.sum(nums)/np.sum(dens) - 2*model.E0)
            # TODO reweighting
            if False:
                p = probability(wf, x)
                h = hamiltonian(wf, x)
                ps = np.append(ps, p)
                hs = np.append(hs, h)
                print(np.sum(hs*ps)/np.sum(ps))
    except KeyboardInterrupt:
        pass

