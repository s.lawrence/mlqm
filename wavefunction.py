from typing import Callable

import equinox as eqx
from equinox import nn
import jax
import jax.numpy as jnp

class MLP(eqx.Module):
    activation: Callable = staticmethod(jax.nn.celu)
    layers: list

    def __init__(self, key, width):
        Nlayers = len(width)-1
        keys = jax.random.split(key, Nlayers)
        self.layers = [nn.Linear(width[n], width[n+1], key=keys[n]) for n in range(Nlayers)]

    def __call__(self, x):
        act = self.activation
        for layer in self.layers[:-1]:
            x = act(layer(x))
        return self.layers[-1](x)

class OddMLP(eqx.Module):
    """
    A multi-layer perceptron with the guarantee that f(x) = -f(-x).
    """
    activation: Callable = staticmethod(jax.nn.tanh)
    layers: list

    def __init__(self, key, width):
        Nlayers = len(width)-1
        keys = jax.random.split(key, Nlayers)
        self.layers = [nn.Linear(width[n], width[n+1], key=keys[n], use_bias=False) for n in range(Nlayers)]

    def __call__(self, x):
        act = self.activation
        for layer in self.layers[:-1]:
            x = act(layer(x))
        return self.layers[-1](x)

class GaussianWavefunction(eqx.Module):
    """ A simple Gaussian. Mostly for testing purposes.
    """
    D: int
    N: int

    def __init__(self, key, D, N):
        self.D = D
        self.N = N

    def __call__(self, x):
        return jnp.exp(jnp.sum(-x**2/2.))

class RadialWavefunction(eqx.Module):
    """ A many-body wavefunction that depends only on the radial coordinates
    and distances.
    """
    D: int
    N: int
    mlp: MLP

    def __init__(self, key, D, N, width, depth):
        self.D = D
        self.N = N
        widths = [N+(N*(N-1))//2] + [width]*depth + [2]
        self.mlp = MLP(key,widths)

    def __call__(self, x):
        assert x.shape == (self.N,self.D)

        N = self.N
        y = jnp.zeros(N+(N*(N-1))//2)
        # Radial coordinates.
        for i in range(N):
            y = y.at[i].set(jnp.linalg.norm(x[i,:]))

        # Distances.
        k = N
        for i in range(N):
            for j in range(i):
                y = y.at[k].set(jnp.linalg.norm(x[i,:]-x[j,:]))
                k = k+1

        z = self.mlp(y)
        return z[0] + 1j*z[1]

class RelativeCoordinates(eqx.Module):
    """

    Only suitable for distinguishable particles.
    """
    def __init__(self, key):
        pass

    def __call__(self, x):
        return x[1:] - x[0]

class ConfinedWavefunction(eqx.Module):
    """ A wavefunction naturally confined in a harmonic potential.

    Args:
        wf: 
        L:

    Returns:
        A wavefunction
    """
    wf: eqx.Module
    L: float

    def __call__(self, x):
        y = self.wf(x)
        r2 = jnp.sum((x/self.L)**2)
        return y * jnp.exp(-r2/2.)

class SumCoordinate(eqx.Module):
    D: int
    N: int
    f: MLP

    def __init__(self, key, D, N):
        self.D = D
        self.N = N
        self.f = MLP(key,[D,D,1])

    def __call__(self, x):
        assert x.shape == (self.N,self.D)
        return jnp.sum(jax.vmap(self.f)(x))

class SumCoordinates(eqx.Module):
    D: int
    N: int
    K: int
    coords: list

    def __init__(self, key, D, N, K):
        """
        Args:
            key: PRNG key for constructing random functions
            D: Spatial dimension
            N: Number of particles
            K: Number of coordinates desired
        """
        self.D = D
        self.N = N
        self.K = K
        keys = jax.random.split(key, K)
        self.coords = [SumCoordinate(k, D, N) for k in keys]

    def __call__(self, x):
        assert x.shape == (self.N,self.D)
        return jnp.array([f(x) for f in self.coords])

class DetCoordinate(eqx.Module):
    """ Totally antisymmetric coordinates, constructed by taking a determinant.

    A single determinental coordinate, of ``N`` particles, is defined by
    constructing ``N`` one-particle coordinates, and creating a matrix in which
    element ``(i,j)`` contains the `i`th coordinate of particle `j`. The
    determinant of that matrix is totally antisymmetric under particle
    exchange.
    """

    D: int
    N: int
    fs: list

    def __init__(self, key, D, N):
        """
        Args:
            key: PRNG key for constructing random functions
            D: Spatial dimension
            N: Number of particles
        """
        self.D = D
        self.N = N
        keys = jax.random.split(key, N)
        self.fs = [MLP(k, [D,D,1]) for k in keys]

    def __call__(self, x):
        assert x.shape == (self.N,self.D)
        def row(f):
            return jax.vmap(f)(x)
        M = jnp.array([row(f) for f in self.fs]).reshape((self.N,self.N))
        return jnp.linalg.det(M)

class DetCoordinates(eqx.Module):
    D: int
    N: int
    K: int
    coords: list

    def __init__(self, key, D, N, K):
        """
        Args:
            key: PRNG key for constructing random functions
            D: Spatial dimension
            N: Number of particles
            K: Number of coordinates desired
        """
        self.D = D
        self.N = N
        self.K = K
        keys = jax.random.split(key, K)
        self.coords = [DetCoordinate(k, D, N) for k in keys]

    def __call__(self, x):
        assert x.shape == (self.N,self.D)
        return jnp.array([f(x) for f in self.coords])

class OneParticleWavefunction(eqx.Module):
    """ Wavefunction of a single particle, in D dimensions.
    """
    mlp: MLP

    def __init__(self, key, N, width, depth):
        widths = [N] + [width]*depth + [2]
        self.mlp = MLP(key,widths)

    def __call__(self, x):
        y = self.mlp(x)
        return y[0]+1j*y[1]

class DistinguishableWavefunction(eqx.Module):
    """
    A distinguishable many-body wavefunction.

    Underneath the hood, this is not really different from
    `OneParticleWavefunction`, except that convenience reshaping is performed.
    """
    mlp: MLP
    D: int
    N: int

    def __init__(self, key, D, N, width, depth):
        self.D, self.N = D, N
        widths = [D*N] + [width]*depth + [2]
        self.mlp = MLP(key,widths)

    def __call__(self, x):
        assert x.shape == (self.N,self.D)
        y = self.mlp(x.reshape((self.D*self.N,)))
        return y[0] + 1j*y[1]

class BosonicWavefunction(eqx.Module):
    """ A wavefunction of many indistinguishable bosons.
    """
    coords: SumCoordinates
    mlp: MLP

    def __init__(self, key, D, N):
        K = D*N
        keyc, keym = jax.random.split(key)
        self.coords = SumCoordinates(keyc, D, N, K)
        self.mlp = MLP(keym, [K,K,2])

    def __call__(self, x):
        y = self.mlp(self.coords(x))
        return y[0] + 1j*y[1]

class FermionicWavefunction(eqx.Module):
    """ A wavefunction of many indistinguishable fermions.
    """
    coords: DetCoordinates
    mlp: OddMLP

    def __init__(self, key, D, N):
        K = D*N
        keyc, keym = jax.random.split(key)
        self.coords = DetCoordinates(keyc, D, N, K)
        self.mlp = OddMLP(keym, [K,K,2])

    def __call__(self, x):
        y = self.mlp(self.coords(x))
        return y[0] + 1j*y[1]

class ManyBodyWavefunction(eqx.Module):
    """ A wavefunction of many particles, with arbitrary distinguishability.

    Each species is labeled by a string. 
    """
    def __init__(self, key, D, particles):
        # TODO
        pass

    def __call__(self, x):
        # TODO
        pass

