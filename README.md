Many approaches to machine learning for quantum mechanics.

* Variationally learning ground states
* Learning time evolution (as solution to a PDE)
* Time evolution via the projection method
* Time evolution via repeated training

