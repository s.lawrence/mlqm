#!/usr/bin/env python

import unittest

import jax
import jax.numpy as jnp

import net
import wavefunction

class TestOddMLP(unittest.TestCase):
    def test_nontrivial(self):
        key1, key2, keyx = jax.random.split(jax.random.PRNGKey(0), 3)
        mlp1 = net.OddMLP(key1, [5,5,5,1])
        mlp2 = net.OddMLP(key2, [5,5,5,1])
        x = jax.random.uniform(keyx, shape=(5,))
        self.assertNotAlmostEqual(mlp1(x), mlp2(x), places=5)

    def test_odd(self):
        keyn, keyx = jax.random.split(jax.random.PRNGKey(0))
        mlp = net.OddMLP(keyn, [5,5,5,1])
        x = jax.random.uniform(keyx, shape=(5,))
        self.assertAlmostEqual(mlp(x), -mlp(-x), places=5)

class TestSumCoordinate(unittest.TestCase):
    def test_nontrivial(self):
        key1, key2, keyx = jax.random.split(jax.random.PRNGKey(0), 3)
        c1 = wavefunction.SumCoordinate(key1, 2, 2)
        c2 = wavefunction.SumCoordinate(key2, 2, 2)
        x = jax.random.uniform(keyx, shape=(2,2))
        self.assertNotAlmostEqual(c1(x), c2(x), places=4)

    def test_symmetric(self):
        N = 5
        for D in [1,2,3]:
            keyc, keyx = jax.random.split(jax.random.PRNGKey(0))
            c = wavefunction.SumCoordinate(keyc, D=D, N=N)
            x = jax.random.uniform(keyx, shape=(N,D))
            for i in range(N):
                for j in range(i):
                    xp = x.at[(i,j),:].set(x[(j,i),:])
                    self.assertAlmostEqual(c(x), c(xp), places=5)

class TestDetCoordinate(unittest.TestCase):
    def test_nontrivial(self):
        for D in [1,2,3]:
            for N in [1,2,3,4]:
                key1, key2, keyx = jax.random.split(jax.random.PRNGKey(0), 3)
                c1 = wavefunction.DetCoordinate(key1, D=D, N=N)
                c2 = wavefunction.DetCoordinate(key2, D=D, N=N)
                x = jax.random.normal(keyx, shape=(N,D))*2
                self.assertNotAlmostEqual(c1(x), c2(x), places=4)

    def test_antisymmetric(self):
        keyc, keyx = jax.random.split(jax.random.PRNGKey(0))
        N = 5
        for D in [1,2,3]:
            keyc, keyx = jax.random.split(jax.random.PRNGKey(0))
            c = wavefunction.DetCoordinate(keyc, D=D, N=N)
            x = jax.random.normal(keyx, shape=(N,D))*2
            for i in range(N):
                for j in range(i):
                    xp = x.at[(i,j),:].set(x[(j,i),:])
                    self.assertAlmostEqual(c(x), -c(xp), places=5)

class TestBosonicWavefunction(unittest.TestCase):
    def test_nontrivial(self):
        key1, key2, keyx = jax.random.split(jax.random.PRNGKey(0), 3)
        c1 = wavefunction.BosonicWavefunction(key1, D=3, N=4)
        c2 = wavefunction.BosonicWavefunction(key2, D=3, N=4)
        x = jax.random.uniform(keyx, shape=(4,3))
        self.assertNotAlmostEqual(c1(x), c2(x), places=4)

    def test_symmetric(self):
        N = 5
        for D in [1,2,3]:
            keywf, keyx = jax.random.split(jax.random.PRNGKey(0))
            wf = wavefunction.BosonicWavefunction(keywf, D=D, N=N)
            x = jax.random.uniform(keyx, shape=(N,D))
            for i in range(N):
                for j in range(i):
                    xp = x.at[(i,j),:].set(x[(j,i),:])
                    self.assertAlmostEqual(wf(x), wf(xp), places=5)

class TestFermionicWavefunction(unittest.TestCase):
    def test_nontrivial(self):
        key1, key2, keyx = jax.random.split(jax.random.PRNGKey(0), 3)
        wf1 = wavefunction.FermionicWavefunction(key1, D=3, N=4)
        wf2 = wavefunction.FermionicWavefunction(key2, D=3, N=4)
        x = jax.random.normal(keyx, shape=(4,3))*2
        self.assertNotAlmostEqual(wf1(x), wf2(x), places=4)

    def test_antisymmetric(self):
        N = 5
        for D in [1,2,3]:
            keywf, keyx = jax.random.split(jax.random.PRNGKey(0))
            wf = wavefunction.FermionicWavefunction(keywf, D=D, N=N)
            x = jax.random.normal(keyx, shape=(N,D))*2
            for i in range(N):
                for j in range(i):
                    xp = x.at[(i,j),:].set(x[(j,i),:])
                    self.assertAlmostEqual(wf(x), -wf(xp), places=5)

class TestManyBodyWavefunction(unittest.TestCase):
    def test_mixed_statistics(self):
        pass

    def test_mixed_nontrivial(self):
        pass

if __name__ == '__main__':
    unittest.main()

