#!/usr/bin/env python

"""
Time evolution via projection.
"""

import argparse

import equinox as eqx
import jax
import jax.numpy as jnp
import optax

import net

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Perform time evolution", fromfile_prefix_chars="@")
    parser.add_argument('--seed', type=int, default=0,
            help="random seed")
    parser.add_argument('--seed-time', action='store_true',
            help="seed PRNG with current time")
    args = parser.parse_args()

    seed = args.seed
    if args.seed_time:
        seed = time.time_ns()
    key = jax.random.PRNGKey(seed)

