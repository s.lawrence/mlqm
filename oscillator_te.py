#!/usr/bin/env python

"""
Time evolution via projection.
"""

import argparse
import functools
import equinox as eqx
import jax
import jax.numpy as jnp
import optax

import model
import wavefunction

jax.config.update('jax_platform_name', 'cpu')

class OscillatorModel(model.Model):
    particles = None

    def __init__(self, D, m, c2, c4):
        self.D = D
        self.m = m
        self.c2 = c2
        self.c4 = c4

    def potential(self, x):
        return self.c2 * jnp.sum(x**2)/2. + self.c4 * jnp.sum(x**4)/4.

    def hamiltonian(self, wf, x):
        D = len(x)
        psi = wf(x)

        # Kinetic
        kin = 0.
        v = jnp.eye(D)
        for i in range(D):
            g1 = lambda y: jax.jvp(wf, (y,), (v[i],))[1]
            g2 = jax.jvp(g1, (x,), (v[i],))[1]
            kin += - psi.conj() * g2 / (2. * self.m)

        # Potential 
        V = self.potential(x)
        pot = psi.conj() * V * psi

        return (kin + pot).real

    def Hwf(self, wf, x):
        D = len(x)
        psi = wf(x)

        # Kinetic
        kin = 0.
        v = jnp.eye(D)
        for i in range(D):
            g1 = lambda y: jax.jvp(wf, (y,), (v[i],))[1]
            g2 = jax.jvp(g1, (x,), (v[i],))[1]
            kin += - g2 / (2. * self.m)

        # Potential 
        V = self.potential(x)
        pot = V * psi

        return kin + pot

    def wavefunction(self, key):
        return OneParticleWavefunction(key, self.D, 10, 4)
        #return Gauss(jnp.array([0.0,1.0]))
        #return Hermite(16)

    def sampler(self, key, wf, N):
        return GaussianMonteCarlo(key, (N, self.D), 2.0)

class OneParticleWavefunction(eqx.Module):
    """ Wavefunction of a single particle, in D dimensions.
    """
    mlp: wavefunction.MLP
    center_r: jax.Array
    center_i:jax.Array
    width_r: jax.Array
    width_i: jax.Array

    def __init__(self, key, N, width, depth):
        widths = [N] + [width]*depth + [2]
        self.mlp = wavefunction.MLP(key,widths)
        self.center_r = jnp.array(0.)
        self.center_i = jnp.array(0.)
        self.width_r = jnp.array(1.)
        self.width_i = jnp.array(1.)

    def __call__(self, x):
        y = self.mlp(x)
        r2r = jnp.sum((x-self.center_r)**2)/self.width_r**2
        r2i = jnp.sum((x-self.center_i)**2)/self.width_i**2
        return y[0] * jnp.exp(-r2r/2) + 1j * y[1] * jnp.exp(-r2i/2)

class Gauss(eqx.Module):
    params: jax.Array
    norm_r: jax.Array
    norm_i: jax.Array

    def __init__(self, params):
        self.params = params
        self.norm_r = jnp.array(1.)
        self.norm_i = jnp.array(0.)

    def __call__(self, x):
        norm = self.norm_r + 1j*self.norm_i
        return norm * jnp.exp(-(x - self.params[0])**2/(2.*self.params[1]**2))


# TODO only for 1D
class Hermite(eqx.Module):
    order: int
    std: jax.Array
    coeffs: jax.Array
    phases: jax.Array

    def __init__(self, order):
        self.order = order
        self.std = jnp.array(1.)
        self.coeffs = jnp.concatenate((jnp.array([1.]), jnp.zeros(self.order-1)))
        self.phases = jnp.zeros(self.order)
        #self.phases = self.phases.at[0].set(jnp.pi/2.)

    def __call__(self, x):
        psi = jnp.exp(1j*self.phases[0]) * jnp.exp(-x**2/(2.*self.std**2))
        y = 1
        for i in range(1, self.order):
            y = y * x
            phase = jnp.exp(1j*self.phases[i])
            psi += phase * self.coeffs[i] * jnp.exp(-x**2/(2.*self.std**2)) * y
        return psi

class GaussianMonteCarlo:
    def __init__(self, key, shape, dev=1.):
        self.key = key
        self.shape = shape
        self.dev = dev

    def __call__(self):
        self.key, key = jax.random.split(self.key)
        x = self.dev * jax.random.normal(key, self.shape)
        def prob(y):
            return jnp.exp(-jnp.sum((y/self.dev)**2)/2.)
        return x, jax.vmap(prob)(x)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Perform time evolution", fromfile_prefix_chars="@")
    parser.add_argument('-d', '--dt', type=float, default=0.01, help="Trotter step")
    parser.add_argument('-fn', '--filename', type=str, default='wave.dat', help='Filename to output amplitude over time')
    parser.add_argument('-lr', '--learningrate', type=float, default=1e-3, help="Learning rate for time evolution")
    parser.add_argument('-i', '--inform', action='store_true', help="Inform the next time step from previous learning")
    parser.add_argument('-oi', '--optinit', action='store_true', help="Initialize optimizer at each time step")
    parser.add_argument('-s', '--nsamples', type=int, default=1<<12, help="Number of samples per training step")
    parser.add_argument('--seed', type=int, default=0,
            help="random seed")
    parser.add_argument('-T', '--time', type=float, default=1.00, help="Total time to evolve")
    parser.add_argument('-t', '--threshold', type=float, default=0.01, help="Threshold on loss function")

    args = parser.parse_args()

    print(args)


    dt = args.dt
    Ns = args.nsamples
    lr = args.learningrate

    seed = args.seed
    key = jax.random.PRNGKey(seed)
    sampleKey, wfKey = jax.random.split(jax.random.PRNGKey(seed), 2)

    model = OscillatorModel(1, 1.0, 1.0, 0.0)
    wf = model.wavefunction(wfKey)
    xf = model.sampler(sampleKey, wf, Ns)
    wf_store = model.wavefunction(wfKey)
    xf_store = model.sampler(sampleKey, wf_store, Ns)
    # Quenching
    model_q = OscillatorModel(1, 1.0, 2.0, 0.0) 

    # ground state preparation with model
    def energy(wf, x, psamp):
        p = jax.vmap(lambda y: jnp.abs(wf(y))**2)(x)
        h = jax.vmap(model.hamiltonian, in_axes=(None,0))(wf, x)

        num = h/psamp
        den = p/psamp
        return jnp.mean(num), jnp.mean(den)

    @eqx.filter_jit
    @functools.partial(eqx.filter_value_and_grad, has_aux=True)
    def loss_energy(wf, x, psamp):
        num, den = energy(wf, x, psamp)
        return num/den, den

    # Initial wave function to prepare
    def iwf(x):
        norm = jnp.power(model.m * jnp.sqrt(model.c2)/jnp.pi, 1/4.)
        return norm * jnp.exp(-model.m * jnp.sqrt(model.c2) * x**2/2.)[0]

    @eqx.filter_jit
    @eqx.filter_value_and_grad #TODO functool.partial() didn't work.
    def loss_iwf(wf, x, psamp):
        psi = jax.vmap(wf)(x)
        psi_iwf = jax.vmap(iwf)(x)
        diff = psi - psi_iwf
        return jnp.mean((diff.conj() * diff / psamp).real) / jnp.mean(psamp)


    # Time evolution's loss function
    @eqx.filter_jit
    @functools.partial(eqx.filter_value_and_grad, has_aux=True)
    def loss_te(wfp, wfc, x, psamp):
        psi = jax.vmap(wfc)(x)
        psip = jax.vmap(wfp)(x)
        Hpsi = jax.vmap(model_q.Hwf, in_axes=(None,0))(wfc, x)
        Hpsip = jax.vmap(model_q.Hwf, in_axes=(None,0))(wfp, x)
        diff = psip/dt - psi/dt + 1j*(Hpsi + Hpsip)/2.
        hp = (Hpsi + Hpsip)/2.
        num = (diff.conj() * diff / psamp).real
        #TODO maybe expand diff in terms and normalize better
        den = (psi.conj() * psi / psamp).real   
        return jnp.mean(num)/jnp.mean(den), None

    opt_g = optax.adam(1e-3)
    opt_g_state = opt_g.init(wf)

    print('Ground state preparation')
    loss = 10
    try:
        while loss > 1e-6:  #TODO    
            x, psamp = xf()
            loss, loss_grad = loss_iwf(wf, x, psamp)
            updates, opt_g_state = jax.jit(opt_g.update)(loss_grad, opt_g_state)      
            wf = eqx.filter_jit(eqx.apply_updates)(wf, updates)
            #print(f'{loss:.6f}')
    except KeyboardInterrupt:
        pass

    rrange = 4.
    rN = 10000
    dx = 2*rrange/float(rN)
    xuni = jnp.linspace(-rrange,rrange,rN).reshape((rN,1))
    def norm(wf):
        psi = jax.vmap(wf)(xuni)
        nor = jnp.sum((psi.conj()*psi*dx).real)
        return nor

    mrange = 4
    mN = 1001
    xms = jnp.linspace(-mrange,mrange,mN).reshape((mN,1))
    def wave(wf):
        psi = jax.vmap(wf)(xms)
        nor = norm(wf)
        amp = (psi.conj()*psi).real
        return (psi/jnp.sqrt(nor)).reshape(mN,), (amp/nor).reshape(mN,)

    amp_store = []
    amp_store.append(xms.reshape(mN,))
    psi, amp = wave(wf)
    amp_store.append(psi.real)
    amp_store.append(psi.imag)

    print('Starting time evolution')
    wf_store = wf
    time = 0
    opt_te = optax.adam(lr)
    opt_te_state = opt_te.init(wf)
    wf_change = jax.tree_util.tree_map(lambda x, y: x - y, wf, wf_store)

    try:
        while time < args.time:
            # Inform next wf from last time step
            if args.inform:
                wf = jax.tree_util.tree_map(lambda x, y: x + y, wf, wf_change)
            x, psamp = xf_store()
            (loss_val_init, _), loss_grad = loss_te(wf, wf_store, x, psamp)

            if args.optinit:
                opt_te_state = opt_te.init(wf)
            loss_val = 100
            counter = 0
            while loss_val > args.threshold:
                x, psamp = xf_store()
                (loss_val, _), loss_grad = loss_te(wf, wf_store, x, psamp)
                if loss_val > args.threshold:
                    updates, opt_te_state = jax.jit(opt_te.update)(loss_grad, opt_te_state)
                    wf = eqx.filter_jit(eqx.apply_updates)(wf, updates)
                #print(f'{time:.4f} {loss_val:.6f}')
                counter += 1

            wf_change = jax.tree_util.tree_map(lambda x, y: x - y, wf, wf_store)
            wf_store = wf
            time = time + dt

            # The average wf parameter and their change
            ave_p = jnp.mean(jnp.abs(jax.flatten_util.ravel_pytree(wf)[0]))
            ave_c = jnp.mean(jnp.abs(jax.flatten_util.ravel_pytree(wf_change)[0]))

            print(f'{time:.4f} {loss_val:.5f} {ave_p:.5f} {ave_c:.5f} {counter} {loss_val_init:.5f}')
            if round(time*100)%10==0: #store every t=0.1 wave function
                psi, amp = wave(wf_store)
                amp_store.append(psi.real)
                amp_store.append(psi.imag)

    except KeyboardInterrupt:
        pass
    
    amp_store = jnp.array(amp_store)
    with open(args.filename, 'w') as f:
        for i in range(len(amp_store[0,:])):
            pr = ' '.join([str('{:.5f}'.format(i)) for i in amp_store[:,i]])
            f.write(pr + '\n')


        
        


